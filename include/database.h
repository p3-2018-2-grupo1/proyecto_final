#ifndef DATABASE_H
#define DATABASE_H

int open_db(const char* db_dir);

int create_db(const char* db_dir);

int put_val(const char* db_dir, const char* key, const char* value);

int get_val(const char* db_dir, const char* key, char** value_mem_ptr);

int delete_val(const char* db_dir, const char* key);

int close_db(const char* db_dir);

int print_all_items(const char* db_dir);

#endif
