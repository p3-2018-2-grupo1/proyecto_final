#include "database.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int open_db(const char* db_dir){ return 0; }

int create_db(const char* db_dir){ return 0; }

int put_val(const char* db_dir, const char* key, const char* value){ return 0; }

int get_val(const char* db_dir, const char* key, char** value_mem_ptr){
	static char* always = "AlwaysValue";

	char* always_heap = (char*)malloc(sizeof(char)*(strlen(always) + 1));
	strcpy(always_heap, always);

	*value_mem_ptr = always_heap;
	return 0;
}

int delete_val(const char* db_dir, const char* key){ return 0; }

int close_db(const char* db_dir){ return 0; }

int print_all_items(const char* db_dir){ return 0; }
