#include "db_server_errors.h"

// Contains error number for database interface. 
int db_server_errno = 0;

const char* db_server_strerror(int db_server_errno){
	switch (db_server_errno){
		case SERVER_SUCCESS:
			return "Exito del servidor.";
		case CANNOT_CREATE_FILE:
			return "No se pudo crear archivo en el servidor para base de datos.";
		case DB_ALREADY_LOADED:
			return "La base de datos requerida ya esta cargada.";
		case DB_DOES_NOT_EXIST:
			return "La base de datos requerida no existe.";
		case DB_NOT_LOADED:
			return "La base de datos requerida no esta cargada.";
		case KEY_DOES_NOT_EXIST:
			return "El dato requerido no existe.";
		case KEY_HAS_NO_VALUE:
			return "El dato requerido no tiene valor asignado (GRAVE).";
		case SIZE_MESSAGE_FAILED:
			return "Fallo el recibir del tamano del nombre de la base de datos.";
		case VALUE_MESSAGE_FAILED:
			return "Fallo el envio del valor.";
		case DB_DIR_RECEIVE_FAILED:
			return "Fallo el recibir del nombre de la base de datos.";
		case NULL_DB_DIR:
			return "Se ha pasado un puntero nulo como argumento de directorio de base.";
		case NULL_DB_KEY:
			return "Se ha pasado un puntero nulo como argumento de llave.";
		case NULL_DB_VALUE:
			return "Se ha pasado un puntero nulo como argumento de valor.";
		case CONNECTION_CLOSE_FAILED:
			return "Fallo el cerrar el descriptor de la conexion con cliente.";
	}

	return "Codigo de error invalido.";
}
