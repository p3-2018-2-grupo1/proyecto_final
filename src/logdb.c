/* --------------------------------------------------------------------------------
 * THE `logdb` PROGRAM
 * This is the server-side program for the log-databse project of the ESPOL
 * System's Programming course, class 2, year 2018. 
 *
 * This program relies heavily on the functionallity implemented by
 * `database.h` and `database.c`, which implement the database-like functionallity
 * required by the project. It also relies on a set of errors defined in 
 * `db_server_errors.h` and `db_server_errors.c`. 
 *
 * Remaining TODO:
 * 	1. 	[DONE] logdb.c: Remove constant-sized buffers in CLI procedures in 
 * 		favor for heap-based containers.
 *
 * 	2. 	[DONE] client-cli.c: Replace `scanf()` in favor for a procedure that 
 * 		can handle white-space delimited strings.
 *
 * 	3. 	clint-logdb.c: improve layout design of code and add meaningful
 * 		comments.
 *
 * 	4. 	client-cli.c: improve layout design of code and add 
 * 		meaningful comments.
 *
 * 	5.	Project-Wide: try to remove all warnings.
 *
 * 	6.	[CRITICAL] Project-Side: add `close_connection()` functionallity and
 * 		make sure that there are no leaks in the project.
 *
 * 	7.	Compile logdb.h and client-logdb.c into a dynamic library.
 *
 * 	8.      [STARTED] Create an automatisized testing program for the whole
 * 		system.
 * -------------------------------------------------------------------------------- */

/* --------------------------------------------------------------------------------
 * INCLUDE DECLARATIONS
 * -------------------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/socket.h>

#include "database.h"
#include "db_server_errors.h"

/* --------------------------------------------------------------------------------
 * MACRO DEFINITIONS
 * -------------------------------------------------------------------------------- */
#define BUFFER_SIZE 1024
#define LISTEN_BACKLOG_SIZE 2048

/* --------------------------------------------------------------------------------
 * FORWARD DECLARATIONS
 * These forward declarations for helper functions defined below (past main()).
 * -------------------------------------------------------------------------------- */

// CLI Procedures
static char* _cli_open_database(int, char*, const char*);
static char* _cli_create_database(int, char*, const char*);
static void _cli_put_value(int, const char*);
static void _cli_get_value(int, const char*);
static void _cli_delete_value(int, const char*);
static void _cli_close_database(int, char*);

// Helper Functions
static int _get_db_name(int, const char*, char**);
static int _get_string(int, char**);
static void _print_chars(const char*);

// --------------------------------------------------------------------------------
// MAIN: 
// This main function has been coded to perform this procedure:
// 	1.- Parse command-line arguments.
// 	2.- Open a passive server connection and socket.
// 	3.- Listen for any willing clients.
// 	4.- Listen to all requests from clients.
// 	5.- Terminate the program when client request so.
// --------------------------------------------------------------------------------
int main(int argc, const char* argv[]){
	
	// Checking for correct number of arguments. 	
	if (argc < 3){
		fprintf(stderr, "Uso: %s <DIRECTORIO-BD> <IP> <PUERTO>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	// Raw parameter values.
	const char* db_directory = argv[1];
	const char* raw_ip       = argv[2];
	int raw_port             = atoi(argv[3]);

	printf("Inicializando servidor.\n");
	printf("Directorio raiz del servidor es: [%s]\n", db_directory);
	
	// Building address structure.
	struct sockaddr_in server_address;
	memset(&server_address, 0, sizeof(server_address));

	server_address.sin_family      = AF_INET;
	server_address.sin_addr.s_addr = inet_addr(raw_ip);
	server_address.sin_port        = htons(raw_port);

	// We create a new socket descriptor.
	int socket_fd = socket(server_address.sin_family, SOCK_STREAM, 0);
	if (socket_fd < 0){
		const char* e_msg = strerror(errno);
		fprintf(stderr, "Error creando descriptor de puerto: %s.\n", e_msg);
		exit(EXIT_FAILURE);
	}

	// We bind the socket to a specific address.
	if (bind(socket_fd, (struct sockaddr*)&server_address, sizeof(server_address))){
		const char* e_msg = strerror(errno);
		fprintf(stderr, "Error enlazando puerto con dirección específica: %s.\n", e_msg);
		close(socket_fd);
		exit(EXIT_FAILURE);
	}

	// We convert our socket from active to passive.
	if (listen(socket_fd, LISTEN_BACKLOG_SIZE)){
		const char* e_msg = strerror(errno);
		fprintf(stderr, "Error convirtiendo puerto a pasivo: %s.\n", e_msg);
		close(socket_fd);
		exit(EXIT_FAILURE);
	}

	// We begin the infinite loop, awaiting for clients to make
	// connections and then handling their requests until they
	// close the connection.


	while (1){
		// We wait for a client connection.
		fprintf(stderr, "\nEsperando conexion del cliente. =========================\n");

		int client_fd = accept(socket_fd, NULL, NULL);
		fprintf(stderr, "Conexion iniciada con cliente.\n");

		// WARNING: For now, we will support one database 
		// open at a time, even though the hash-database
		// engine supports several open databases at a time.
		// This variable holds the currently-opened database
		// directory path.
		char* db_full_path = NULL;
		while (1){
			fprintf(stderr, "\nEsperando siguiente operacion. ----------------- \n");


			// Get the size of the next operation to perform.
			uint32_t type_len;
			ssize_t num_read = read(client_fd, &type_len, sizeof(uint32_t));
			type_len         = ntohl(type_len);

			// If we didn't read any data, client has closed session.	
			if (num_read == 0) {
				close(client_fd);
				break;
			}

			fprintf(stderr, "-> Tamano del comando de la siguiente operacion es: %u\n", type_len);

			// Get string informing next operation to perform.
			char action_buffer[BUFFER_SIZE];
			memset(action_buffer, 0, BUFFER_SIZE - 1);

			num_read = read(client_fd, action_buffer, type_len);
			fprintf(stderr, "-> Siguiente operacion es: %s\n", action_buffer);

			if (strcmp(action_buffer, "OPEN_DB") == 0){
				db_full_path = _cli_open_database(client_fd, db_full_path, db_directory);
			} else if (strcmp(action_buffer, "CREATE_DB") == 0){
				db_full_path = _cli_create_database(client_fd, db_full_path, db_directory);
			} else if(strcmp(action_buffer, "PUT_VAL") == 0){
				_cli_put_value(client_fd, db_full_path);
			} else if (strcmp(action_buffer, "GET_VAL") == 0){
				_cli_get_value(client_fd, db_full_path);
			} else if(strcmp(action_buffer, "DELETE") == 0){
				_cli_delete_value(client_fd, db_full_path);
			} else if(strcmp(action_buffer, "CLOSE_DB") == 0){
				_cli_close_database(client_fd, db_full_path);
				break;
			}
		}
	}

	// Close socket to avoid leaks.
	close(socket_fd);

	return 0;
}

/* --------------------------------------------------------------------------------
 * CLI (Command-Line Interface) PROCEDURE DEFINITIONS
 * These are the recepy-like procedures which prescribe the protocol
 * between the client and server when the client has asked to perform
 * one specific action.
 * -------------------------------------------------------------------------------- */
static char* _cli_create_database(int client_fd, char* db_full_path, const char* db_directory){

	// Create dabase request.
	fprintf(stderr, "-> Requisito es: crear nueva base de datos y abrirla.\n");
	
	// Save old path for release if opening new database was successful.
	char* old_path = db_full_path;
	char* new_path = NULL;

	int result = _get_db_name(client_fd, db_directory, &new_path);

	if (result == 0) 
		result = create_db(new_path);

	// If creation failed, restore previously-opened database.
	if (result == 0){
		db_full_path = new_path;
		fprintf(stderr, "-> Se creo la base de datos [%s] con exito.\n", new_path);
	} else {
		const char* e_msg = db_server_strerror(db_server_errno);
		fprintf(stderr, "-> No se pudo crear ni abrir la base de datos [%s]: %s\n",
				new_path, e_msg);
	}
	

	// Close previously opened database.
	if (result == 0 && old_path != NULL && strcmp(new_path, old_path) != 0){
		fprintf(stderr, "-> Cerrando base previamente abierta\n");
		result = close_db(old_path);

		if (result == 0){
			free(old_path);
		} else {
			const char* e_msg = db_server_strerror(db_server_errno);
			fprintf(stderr, "-> No se pudo cerrar la base de datos previamente abierta [%s]: %s\n",
					old_path, e_msg);
			free(new_path);
			db_full_path = old_path;
		}
	}

	// Returning result to client.
	int n_db_server_errno = htonl(db_server_errno);
	write(client_fd, &n_db_server_errno, sizeof(int));


	return db_full_path;
}
// --------------------------------------------------------------------------------
static char* _cli_open_database(int client_fd, char* db_full_path, const char* db_directory){

	// Open database request.
	fprintf(stderr, "-> Requisito es: abrir base de datos.\n");

	// Save old path for release if opening new database was successful.
	char* old_path = db_full_path;
	char* new_path = NULL;

	int result = _get_db_name(client_fd, db_directory, &new_path);

	// If there were no errors retrieving db name, we open database.
	if (result == 0)
		result = open_db(new_path);
	
	// If opening failed, restore previously-opened database.
	if (result == 0){
		db_full_path = new_path;
		fprintf(stderr, "-> Se abrio base de datos [%s] con exito.\n", new_path);
	} else {
		const char* e_msg = db_server_strerror(db_server_errno);
		fprintf(stderr, "-> No se pudo abrir base de datos [%s]: %s\n", new_path, e_msg);
	}


	// Close previously opened database.
	if (result == 0 && old_path != NULL && strcmp(new_path, old_path) != 0){
		fprintf(stderr, "-> Cerrando base previamente abierta\n");
		result = close_db(old_path);

		if (result == 0){
			free(old_path);
		} else {
			const char* e_msg = db_server_strerror(db_server_errno);
			fprintf(stderr, "-> No se pudo cerrar la base de datos previamente abierta [%s]: %s\n",
					old_path, e_msg);

			free(new_path);
			db_full_path = old_path;
		}
	}
	
	// Returning result to client.
	int n_db_server_errno = htonl(db_server_errno);
	write(client_fd, &n_db_server_errno, sizeof(int));

	return db_full_path;
}


// --------------------------------------------------------------------------------
static void _cli_put_value(int client_fd, const char* db_full_path){

	// Put a value.
	fprintf(stderr, "-> Requisito es: insertar un valor.\n");

	// Get key first.
	char* key_buffer = NULL;
	int result       = _get_string(client_fd, &key_buffer);

	// Get value next.
	char* value_buffer = NULL;
	if (result == 0)
		result = _get_string(client_fd, &value_buffer);

	// If all's good, insert value in database.
	if (result == 0)
		result = put_val(db_full_path, key_buffer, value_buffer);

	if (result == 0){
		fprintf(stderr, "-> Se agrego el valor [%s]:[%s] con exito.\n",
				key_buffer, value_buffer);
	} else {
		const char* e_msg = db_server_strerror(db_server_errno);
		fprintf( stderr, "-> No se pudo agregar el valor [%s]:[%s] con exito: %s\n",
				key_buffer, value_buffer, e_msg);
	}

	// Returning result to client.
	int n_db_server_errno = htonl(db_server_errno);
	write(client_fd, &n_db_server_errno, sizeof(int));

	free(key_buffer);
	free(value_buffer);

	return;
}

// --------------------------------------------------------------------------------
static void _cli_get_value(int client_fd, const char* db_full_path){

	// Get a value.
	fprintf(stderr, "-> Requisito es: obtener un valor.\n");

	// Get key first.
	char* key_buffer = NULL;
	int result;
	int gs_result;

       	gs_result = result = _get_string(client_fd, &key_buffer);

	// If all's good, send value.
	int gv_result;
	char* value_buffer = NULL;
	if (result == 0)
		gv_result = result = get_val(db_full_path, key_buffer, &value_buffer);

	// Returning result to client.
	int n_db_server_errno = htonl(db_server_errno);
	write(client_fd, &n_db_server_errno, sizeof(int));

	uint32_t value_len;
	if (result == 0){
		// We send size of string.
		value_len   = (uint32_t)(1 + strlen(value_buffer));
		fprintf(stderr, "-> Enviando tamano de valor: %u.\n", value_len);

		uint32_t n_value_len = htonl(value_len);
		ssize_t n_written    = write(client_fd, &n_value_len, sizeof(uint32_t));

		if (n_written < sizeof(uint32_t)){
			db_server_errno = SIZE_MESSAGE_FAILED;
			result = -1;
		}
	}

	if (result == 0){
		fprintf(stderr, "-> Enviando valor: [%s].\n", value_buffer);
		
		ssize_t n_written = write(client_fd, value_buffer, value_len);

		if (n_written < value_len){
			db_server_errno = VALUE_MESSAGE_FAILED;
			result = -1;
		}
	}

	if (result == 0){
		fprintf(stderr, "-> Se envio el valor [%s]:[%s] con exito.\n",
				key_buffer, value_buffer);

	} else {
		const char* e_msg = db_server_strerror(db_server_errno);
		fprintf(stderr, "-> No se pudo enviar el valor [%s]:[%s] con exito: %s\n",
				key_buffer, value_buffer, e_msg);
	}

	// Only delete when corresponding operations where successful.
	if (gs_result == 0) free(value_buffer);
	if (gv_result == 0) free(key_buffer);

	return;
}

// --------------------------------------------------------------------------------
static void _cli_delete_value(int client_fd, const char* db_full_path){

	// Delete a value.
	fprintf(stderr, "-> Requisito es: eliminar un valor.\n");

	// Get key first.
	char* key_buffer = NULL;
	int result       = _get_string(client_fd, &key_buffer);
	
	if (result == 0) 
		result = delete_val(db_full_path, key_buffer);

	if (result == 0){
		db_server_errno = 0;
		fprintf(stderr, "-> Se elimino el valor [%s] con exito.\n", key_buffer);
	} else {
		const char* e_msg = db_server_strerror(db_server_errno);
		fprintf( stderr, "-> No se pudo eliminar [%s]: %s\n", key_buffer, e_msg);
	}
	
	// Returning result to client.
	int n_db_server_errno = htonl(db_server_errno);
	write(client_fd, &n_db_server_errno, sizeof(int));

	free(key_buffer);

	return;
}

// --------------------------------------------------------------------------------
static void _cli_close_database(int client_fd, char* db_full_path){

	// Close 
	fprintf(stderr, "-> Requisito es: cerrar base de datos y desconectar del cliente.\n");

	int result = close_db(db_full_path);

	if (result == 0){
		fprintf(stderr, "-> Se cerro la base de datos [%s] con exito.\n", db_full_path);
	} else {
		const char* e_msg = db_server_strerror(db_server_errno);
		fprintf( stderr, "-> No se pudo cerrar la base de datos [%s]: %s\n", db_full_path, e_msg);
	}

	// Returning result to client.
	int n_db_server_errno = htonl(db_server_errno);
	write(client_fd, &n_db_server_errno, sizeof(int));

	free(db_full_path);

	close(client_fd);
	fprintf(stderr, "Se desconecto del cliente con exito.\n");
}

/* --------------------------------------------------------------------------------
 * HELPER DEFINITIONS: 
 * These are some helper functions to make rutinary socket queries easier to re-use.
 * -------------------------------------------------------------------------------- */
static int _get_db_name(int client_fd, const char* db_directory, char** result_buffer){
	// Need size of database name. 
	fprintf(stderr, "-> Esperando tamano del nombre de la base de datos.\n");
	
	uint32_t db_name_len;
	ssize_t num_read = read(client_fd, &db_name_len, sizeof(uint32_t));
	db_name_len      = ntohl(db_name_len);

	if (num_read < sizeof(uint32_t)){
		db_server_errno = SIZE_MESSAGE_FAILED;
		return -1;
	}

	fprintf(stderr, "-> Tamano de nombre de base de datos es: %u\n", db_name_len);

	// We create enough space for the directory string.
	char* db_name_buffer = (char*)malloc(sizeof(char) * db_name_len);

	// Processing dabase name.
	num_read = read(client_fd, db_name_buffer, db_name_len);
	if (num_read < db_name_len){
		db_server_errno = DB_DIR_RECEIVE_FAILED;
		free(db_name_buffer);
		return -1;
	}

	fprintf(stderr, "-> Nombre de base de datos es: %s\n", db_name_buffer);

	// Building database directory name.
	*result_buffer = (char*)malloc(sizeof(char) * (strlen(db_directory) + db_name_len + 1));
 
	sprintf(*result_buffer, "%s/%s", db_directory, db_name_buffer);
	fprintf(stderr, "-> Directorio completo de base de datos es: %s\n", *result_buffer);

	free(db_name_buffer);

	return 0;
}

// --------------------------------------------------------------------------------
static int _get_string(int client_fd, char** result_buffer){

	// First, we need the size of the string.
	fprintf(stderr, "-> Esperando tamano de la cadena de caracteres.\n");

	uint32_t str_len;
	ssize_t num_read = read(client_fd, &str_len, sizeof(uint32_t));
	str_len          = ntohl(str_len);

	if (num_read < sizeof(uint32_t)){
		db_server_errno = SIZE_MESSAGE_FAILED;
		return -1;
	}

	fprintf(stderr, "-> Tamano de cadena de caracteres es: [%u]\n", str_len);
	
	// Acquiring enough space for incoming string.
	*result_buffer = (char*) malloc(sizeof(char)*str_len);	
		
	// Getting string.
	num_read = read(client_fd, *result_buffer, str_len);
	if (num_read < str_len){
		db_server_errno = DB_DIR_RECEIVE_FAILED;
		free(result_buffer);
		return -1;
	}

	fprintf(stderr, "-> Cadena de caracteres es: [%s]\n", *result_buffer);

	return 0;

}

static void _print_chars(const char* str){
	int i = 0;
	fprintf(stderr, "\n");
	while (str != NULL && str[i] != '\0'){
		fprintf(stderr, "%c", str[i++]);
	}
	fprintf(stderr, "\n");

}
