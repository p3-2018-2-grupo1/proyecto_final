
/* --------------------------------------------------------------------------------
 * INCLUDE DECLARATIONS
 * -------------------------------------------------------------------------------- */
#include "uthash.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/socket.h>

#include "logdb.h"
#include "db_client_errors.h"
#include "db_server_errors.h"

/* --------------------------------------------------------------------------------
 * MACRO DEFINITIONS
 * -------------------------------------------------------------------------------- */
#define MAX_SLEEP 64

conexionlogdb* conectar_db(const char *ip, int puerto){

	// Building address structure.
	struct sockaddr_in client_address;
	memset(&client_address, 0, sizeof(client_address));

	client_address.sin_family      = AF_INET;
	client_address.sin_addr.s_addr = inet_addr(ip);
	client_address.sin_port        = htons(puerto);

	// We create a socket descriptor, try to connect to server. Retry after
	// sleep several times.
	int socket_fd;
	int num_sec = 1;
	while (1) {
		if ((socket_fd = socket(client_address.sin_family, SOCK_STREAM, 0)) < 0){
			const char* e_msg = strerror(errno);
			fprintf(stderr, "Error creando descriptor de puerto: %s.\n", e_msg);
			exit(EXIT_FAILURE);
		}

		int connect_result = connect(
					socket_fd, 
					(struct sockaddr*)&client_address,
					sizeof(client_address));

		if (connect_result == 0){ break; } 

		// If we didn't break, then connect was unsuccesful.
		const char* e_msg = strerror(errno);
		fprintf(stderr, "Error al iniciar conexion con servidor: %s.\n", e_msg);
		if (num_sec > MAX_SLEEP){
			fprintf(stderr, "Maximo numero de intentos alcanzados. Abortando. \n");
			exit(EXIT_FAILURE);
		} 			
		
		// Close invalid socket, try again.	
		fprintf(stderr, "Reintentando.\n");
		close(socket_fd);

		sleep(num_sec);
		num_sec <<= 1;
	}

	conexionlogdb* logdb = (conexionlogdb*)malloc(sizeof(conexionlogdb));

	logdb->ip = (char*)malloc(sizeof(char)*(strlen(ip) + 1));
	strcpy(logdb->ip, ip);

	logdb->puerto    = puerto;
	logdb->id_sesion = 1;
	logdb->nombredb  = NULL;
	logdb->sockfd  = socket_fd;

	return logdb;
}	

// --------------------------------------------------------------------------------
static int _send_request_type(conexionlogdb* connection, const char* connection_type){
	
	// We must first send the size of the type of connection to perform.
	fprintf(stderr, "-> Enviando tamano de tipo del requisito.\n");

	uint32_t type_len  = (uint32_t)(1 + strlen(connection_type));
	uint32_t ntype_len = htonl(type_len);

	ssize_t n_written = write(
			connection->sockfd,
			&ntype_len,
			sizeof(uint32_t));

	if (n_written < sizeof(uint32_t)){
		db_client_errno = FAILED_SIZE_SEND;
		return -1;
	}
			
	// Next we send the type of connection.
	fprintf(stderr, "-> Enviando tipo de requisito.\n");
	n_written = write(
		connection->sockfd, 
		connection_type, 
		type_len);

	if (n_written < type_len){
		db_client_errno = FAILED_TYPE_SEND;
		return -1;
	}	

	return 0;
}

// --------------------------------------------------------------------------------
static int _send_database_name(conexionlogdb* connection, const char* db_name){
	
	// We send the size of the dabase directory string.
	fprintf(stderr, "-> Enviando tamano de nombre de base de datos.\n");
	uint32_t db_len   = (uint32_t)(1 + strlen(db_name));

	uint32_t n_db_len = htonl(db_len);
	ssize_t n_written = write(connection->sockfd, &n_db_len, sizeof(uint32_t));

	if (n_written < sizeof(uint32_t)){
		db_client_errno = FAILED_SIZE_SEND;
		return -1;
	}

	// Send database directory string.
	fprintf(stderr, "-> Enviando nombre de base de datos.\n");
	
	n_written = write(connection->sockfd, db_name, db_len);

	if (n_written < db_len){
		db_client_errno = FAILED_ARG_SEND;
		return -1;
	}

	return 0;
}

// --------------------------------------------------------------------------------
static int _send_string(conexionlogdb* connection, const char* str){
	
	// We send the size of string.
	uint32_t str_len   = (uint32_t)(1 + strlen(str));
	fprintf(stderr, "-> Enviando tamano de valor: %u.\n", str_len);

	uint32_t n_str_len = htonl(str_len);
	ssize_t n_written  = write(connection->sockfd, &n_str_len, sizeof(uint32_t));

	if (n_written < sizeof(uint32_t)){
		db_client_errno = FAILED_SIZE_SEND;
		return -1;
	}

	// Send string.
	fprintf(stderr, "-> Enviando valor: [%s].\n", str);
	
	n_written = write(connection->sockfd, str, str_len);

	if (n_written < str_len){
		db_client_errno = FAILED_ARG_SEND;
		return -1;
	}

	return 0;
}

// --------------------------------------------------------------------------------
static int _get_server_result(conexionlogdb* connection){
	fprintf(stderr, "-> Esperando respuesta del servidor.\n");

	int server_result;
	ssize_t num_read = read(connection->sockfd, &server_result, sizeof(int));
	server_result    = ntohl(server_result);

	if (num_read < sizeof(server_result)){
		db_client_errno = FAILED_RESULT_RECEIVE;
		return -1;
	}

	if (server_result != 0){
		db_client_errno = SERVER_ERROR;
		db_server_errno = server_result;
		return -1;
	}

	return 0;
}

// --------------------------------------------------------------------------------
int crear_db(conexionlogdb *conexion, char *nombre_db){
	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return -1;		
	}

	// We first must send the type of request to be made.
	int result = _send_request_type(conexion, "CREATE_DB");
	if (result < 0) return result;

	// We then send the database name: the only arg needed to open 
	// a database.
	result = _send_database_name(conexion, nombre_db);
	if (result < 0) return result;

	// Get server response. 
	result = _get_server_result(conexion);
	if (result < 0) return result;

	// If successful, change database name in TDA.
	if (conexion->nombredb != NULL)
		free(conexion->nombredb);

	conexion->nombredb = (char*)malloc(sizeof(char)*strlen(nombre_db));
	strcpy(conexion->nombredb, nombre_db);

	return 0;
}


// --------------------------------------------------------------------------------
int abrir_db(conexionlogdb *conexion, char *nombre_db){

	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return -1;		
	}

	// We first must send the type of request to be made.
	int result = _send_request_type(conexion, "OPEN_DB");
	if (result < 0) return result;

	// We then send the database name: the only arg needed to open 
	// a database.
	result = _send_database_name(conexion, nombre_db);
	if (result < 0) return result;

	// Get server response.
	result = _get_server_result(conexion);
	if (result < 0) return result;

	// If successful, change database name in TDA.
	if (conexion->nombredb != NULL)
		free(conexion->nombredb);

	conexion->nombredb = (char*)malloc(sizeof(char)*strlen(nombre_db));
	strcpy(conexion->nombredb, nombre_db);
	
	return 0;
}

// --------------------------------------------------------------------------------
int put_val(conexionlogdb *conexion, char* clave, char* valor){

	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return -1;		
	}

	if (conexion->nombredb == NULL){
		db_client_errno = NO_OPEN_DB;
		return -1;
	}

	// We first must send the type of request to be made.
	int result = _send_request_type(conexion, "PUT_VAL");
	if (result < 0) return result;

	// We send the key first.
	result = _send_string(conexion, clave);
	if (result < 0) return result;

	// We then send the value.
	result = _send_string(conexion, valor);
	if (result < 0) return result;

	return _get_server_result(conexion);
}

// --------------------------------------------------------------------------------
char* get_val(conexionlogdb* conexion, char* clave){

	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return NULL;		
	}

	if (conexion->nombredb == NULL){
		db_client_errno = NO_OPEN_DB;
		return NULL;
	}

	// We first must send the type of request to be made.
	int result = _send_request_type(conexion, "GET_VAL");
	if (result < 0) return NULL;

	// Send the key. ------------------------------
	result = _send_string(conexion, clave);
	if (result < 0) return NULL;

	// Check if key retrieval was successful ------
	result = _get_server_result(conexion);
	if (result < 0) return NULL;

	// If it was successful...	
	//
	// Get value. ---------------------------------
	// First, we need the size of the incoming value.
	fprintf(stderr, "-> Esperando tamano de valor.\n");

	uint32_t value_len;
	ssize_t num_read = read(conexion->sockfd, &value_len, sizeof(uint32_t));
	value_len        = ntohl(value_len);

	if (num_read < sizeof(uint32_t)){
		db_client_errno = FAILED_SIZE_RECEIVE;
		return NULL;
	}

	fprintf(stderr, "-> Tamano del valor es: [%u]\n", value_len);

	// Getting string.
	char* value_str = (char*)malloc(sizeof(char)*value_len);
	num_read = read(conexion->sockfd, value_str, value_len);

	if (num_read < value_len){
		db_client_errno = FAILED_RESULT_RECEIVE;
		free(value_str);
		return NULL;
	}

	return value_str;
}

// --------------------------------------------------------------------------------
int eliminar(conexionlogdb *conexion, char *clave){

	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return -1;		
	}

	if (conexion->nombredb == NULL){
		db_client_errno = NO_OPEN_DB;
		return -1;
	}

	// Send type of request.
	int result = _send_request_type(conexion, "DELETE");
	if (result < 0) return -1;

	// Send the key. ------------------------------
	result = _send_string(conexion, clave);
	if (result < 0) return -1;
	
	// Check if key deletion was successful -------
	return _get_server_result(conexion);
	

}

// --------------------------------------------------------------------------------
void cerrar_db(conexionlogdb *conexion){
	db_client_errno = 0;
	
	if (conexion == NULL || conexion->id_sesion < 0){
		db_client_errno = NO_CONNECTION;
		return;		
	}

	if (conexion->nombredb != NULL){

		// Send type of request.
		int result = _send_request_type(conexion, "CLOSE_DB");
		if (result < 0) return;

		// We send dabase name.
		result = _send_database_name(conexion, conexion->nombredb);
		if (result < 0) return;
		
		result = _get_server_result(conexion);
		if (result < 0) return;

		free(conexion->nombredb);
		conexion->nombredb = NULL;
	}

	_send_request_type(conexion, "DISCONNECT");
	close(conexion->sockfd);

	free(conexion->ip);
	free(conexion);
	
	return;
}


