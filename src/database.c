// --------------------------------------------------------------------------------
#include "database.h"
#include "db_server_errors.h"

#include "uthash.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

// --------------------------------------------------------------------------------

const char* TOMBSTONE_DELETE = "@T!";
const char* TOMBSTONE_HEADER = "@INIT!:@NA";

// --------------------------------------------------------------------------------
// This struct represents the entries of a database. 
typedef struct database_entries_hash {
	char* key;
	size_t offset;
	UT_hash_handle hh;
} Entries_Hash;

// This struct represents all opened databased.
typedef struct databases_dir_hash {
	char* db_dir;
	Entries_Hash* entries_hash_table;
	UT_hash_handle hh;
} Databases_Hash;

// --------------------------------------------------------------------------------
static Databases_Hash* db_hash_table = NULL;

// --------------------------------------------------------------------------------
int create_db(const char* db_dir){

	db_server_errno = 0;

	if (db_dir == NULL){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	// First we check whether database is already loaded.
	Databases_Hash* dbh = NULL;
	HASH_FIND( hh, db_hash_table, db_dir, strlen(db_dir) + 1, dbh);

	if (dbh){
		db_server_errno = DB_ALREADY_LOADED;
		return -1;		
	}

	// Next, we check whether database exists.
	if (access(db_dir, F_OK) == -1){
		// If file does not exist, we create file.
	
		// Set umask to 0 bit-field.	
		umask(0);

		// We open the output file in write-only mode.
		int openFlags = O_CREAT | O_WRONLY;
		
		// Set the permissions to rw-rw-rw
		int filePerms = S_IRUSR | S_IWUSR  | S_IRGRP | S_IWGRP |
				S_IROTH | S_IWOTH;

		int db_fd = open(db_dir, openFlags, filePerms);
		if (db_fd == -1){
			db_server_errno = CANNOT_CREATE_FILE;
			return -1;
		}
		
		// Write standard header tombstone value.
		write(db_fd, TOMBSTONE_HEADER, strlen(TOMBSTONE_HEADER));
		write(db_fd, "\n", 1);

		close(db_fd);
	}

	return open_db(db_dir);
}

// --------------------------------------------------------------------------------
#define INPUT_BUFFER_SIZE 1048576

enum parsing_status {
	LOADING_KEY,
	LOADING_VALUE,
	FINISHED
};

int open_db(const char* db_dir){

	db_server_errno = 0;

	if (db_dir == NULL){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	// First we check whether database is already loaded.
	Databases_Hash* dbh = NULL;
	HASH_FIND( hh, db_hash_table, db_dir, strlen(db_dir) + 1, dbh);

	if (dbh){
		db_server_errno = DB_ALREADY_LOADED;
		return -1;		
	}

	// Next we check whether dabase exists by opening it.
	int open_flags = O_RDONLY;
	
	int db_fd = open(db_dir, open_flags, 0);
	if (db_fd < 0){
		db_server_errno = DB_DOES_NOT_EXIST;
		return -1;
	}

	// If dabase file does exists, we create a new 
	// hash to hold the entries.

	// We iterate over the database file to construct
	// the hash of offsets.	
	enum parsing_status status = LOADING_KEY;

	int bytes_read     = 0;
	int new_val_offset = 0;

	char char_buffer   = 0;

	char key_buffer[INPUT_BUFFER_SIZE];	
	memset(key_buffer, 0, INPUT_BUFFER_SIZE);

	char value_buffer[INPUT_BUFFER_SIZE];	
	memset(value_buffer, 0, INPUT_BUFFER_SIZE);

	int i = 0;

	Entries_Hash* opened_hash_table = NULL;

	while (read(db_fd, &char_buffer, 1)){
		bytes_read++;

		if (char_buffer == ':'){

			key_buffer[i] = '\0';
			i             = 0;
			status        = LOADING_VALUE;

			continue;
		} 
		
		if (char_buffer == '\n'){
			Entries_Hash* to_insert = NULL;

			size_t key_len = strlen(key_buffer) + 1;
			HASH_FIND( 
				hh, 
				opened_hash_table,
				key_buffer,
				key_len,
				to_insert);

			if (to_insert == NULL){

				to_insert = (Entries_Hash*) malloc(sizeof(Entries_Hash));

				size_t key_len = strlen(key_buffer) + 1;
				char* key      = (char*) malloc(sizeof(char) * key_len);
				strcpy(key, key_buffer);

				to_insert->key    = key;

				HASH_ADD_KEYPTR( hh, opened_hash_table, 
						 to_insert->key, 
						 key_len, 
						 to_insert);

			}

			to_insert->offset = new_val_offset;

			// If tombstone value found, remove from hash table.
			if (strcmp(value_buffer, TOMBSTONE_DELETE) == 0){
				HASH_DEL(opened_hash_table, to_insert);
				free(to_insert->key);
				free(to_insert);
			}

			memset(key_buffer, 0, INPUT_BUFFER_SIZE);
			memset(value_buffer, 0, INPUT_BUFFER_SIZE);

			i              = 0;
			status         = LOADING_KEY;
			new_val_offset = bytes_read;
			
			continue;
		}

		if (status == LOADING_KEY){
			key_buffer[i++] = char_buffer;
		} else if (status == LOADING_VALUE) {
			value_buffer[i++] = char_buffer;
		}	

	}
	
	// We assign the newly-created hash to the database hash collection.
	size_t db_dir_len = strlen(db_dir) + 1;
	char* db_dir_key  = (char*) malloc(sizeof(char) * db_dir_len);
	memset(db_dir_key, 0, db_dir_len);

	strcpy(db_dir_key, db_dir);

	dbh                     = (Databases_Hash*) malloc(sizeof(Databases_Hash));
	dbh->db_dir             = db_dir_key;
	dbh->entries_hash_table = opened_hash_table;

	HASH_ADD_KEYPTR( hh, db_hash_table, dbh->db_dir, db_dir_len, dbh);

	// Close the database file.
	close(db_fd);

	return 0;
}

// --------------------------------------------------------------------------------
int put_val(const char* db_dir, const char* key, const char* value){

	db_server_errno = 0;

	if (db_dir == NULL){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	if (key == NULL){
		db_server_errno = NULL_DB_KEY;
		return -1;
	}

	if (value == NULL){
		db_server_errno = NULL_DB_VALUE;
		return -1;
	}

	// First we check whether database is already loaded.
	Databases_Hash* dbh = NULL;
	HASH_FIND( hh, db_hash_table, db_dir, strlen(db_dir) + 1, dbh);

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;	
	}
	
	// Next we open the dabase file.
	int open_flags = O_WRONLY;
	
	int db_fd = open(db_dir, open_flags, 0);
	if (db_fd < 0){
		db_server_errno = DB_DOES_NOT_EXIST;
		return -1;
	}

	// We move the file's cursor to the end of the file.
	off_t offset = lseek(db_fd, 0, SEEK_END);

	// Write new entry to the file.
	write(db_fd, key, strlen(key));
	write(db_fd, ":", 1);
	write(db_fd, value, strlen(value));
	write(db_fd, "\n", 1);

	// We close the file.
	close(db_fd);

	// We add a new entry in the hash-table.
	Entries_Hash* target_hash_table = dbh->entries_hash_table;
	Entries_Hash* to_insert         = NULL;

	size_t key_len = strlen(key) + 1;
	HASH_FIND( hh, target_hash_table, key, key_len, to_insert);

	if (to_insert == NULL){
		to_insert = (Entries_Hash*) malloc(sizeof(Entries_Hash));

		char* new_key  = (char*) malloc(sizeof(char) * key_len);
		strcpy(new_key, key);

		to_insert->key = new_key;

		HASH_ADD_KEYPTR( hh, target_hash_table,
				 to_insert->key, 
				 key_len, 
				 to_insert);
	}

	to_insert->offset = offset;
	
	return 0;
}

// --------------------------------------------------------------------------------
int get_val(const char* db_dir, const char* key, char** value_mem_ptr){

	db_server_errno = 0;

	if (db_dir == NULL){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	if (key == NULL){
		db_server_errno = NULL_DB_KEY;
		return -1;
	}

	Databases_Hash* dbh = NULL;

	// First we check whether database is already loaded.
	HASH_FIND( hh, db_hash_table, db_dir, strlen(db_dir) + 1, dbh);

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;		
	}

	// Next we query the hash to see whether key exists.
	Entries_Hash* table_to_query = dbh->entries_hash_table;
	Entries_Hash* find_container = NULL;

	size_t key_len = strlen(key) + 1;
	HASH_FIND( 
		hh, 
		table_to_query,
		key,
		key_len,
		find_container);

	if (!find_container){
		db_server_errno = KEY_DOES_NOT_EXIST;	
		return -1;
	}

	// Next we open the database file. 
	int open_flags = O_RDONLY;
	int db_fd      = open(db_dir, open_flags, 0);
	if (db_fd < 0){
		db_server_errno = DB_DOES_NOT_EXIST;
		return -1;
	}

	// We jump to appropriate entry location.
	size_t offset = find_container->offset;
	lseek(db_fd, offset, SEEK_SET);
	
	// We retrieve the entry.
	enum parsing_status status = LOADING_KEY;

	char char_buffer = 0;
	char line_buffer[INPUT_BUFFER_SIZE];
	memset(line_buffer, 0, INPUT_BUFFER_SIZE);

	int i = 0;

	while (read(db_fd, &char_buffer, 1)){
		if (char_buffer == ':'){
			status = LOADING_VALUE;
			continue;
		}

		if (char_buffer == '\n'){
			line_buffer[i]     = '\0';
			char* value_mem    = (char*)malloc(sizeof(char)*(i + 1));
			strcpy(value_mem, line_buffer);
		
			*value_mem_ptr     = value_mem;	
			return 0;
		}

		if (status == LOADING_KEY) continue;  
		
		if (status == LOADING_VALUE) {
			// Hack: limiting value's size to INPUT_BUFFER_SIZE
			if (i < INPUT_BUFFER_SIZE - 1){
				line_buffer[i++] = char_buffer;
			}
		}
	}

	db_server_errno = KEY_HAS_NO_VALUE;
	return -1;
}
// --------------------------------------------------------------------------------
int delete_val(const char* db_dir, const char* key){

	db_server_errno = 0;

	if (db_dir == NULL){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	if (key == NULL){
		db_server_errno = NULL_DB_KEY;
		return -1;
	}

	Databases_Hash* dbh = NULL;

	// First we check whether database is already loaded.
	HASH_FIND( hh, db_hash_table, db_dir, strlen(db_dir) + 1, dbh);

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;		
	}

	// We check whether entry exists in database.
	Entries_Hash* target_hash_table = dbh->entries_hash_table;
	Entries_Hash* to_delete         = NULL;

	HASH_FIND( hh, target_hash_table, key, strlen(key) + 1, to_delete);

	if (to_delete == NULL){
		db_server_errno = KEY_DOES_NOT_EXIST;
		return -1;
	}

	// If it does exist, remove it from the hash.
	HASH_DELETE( hh, target_hash_table, to_delete);

	// We also need to add a tombstone entry in the file.
	int open_flags = O_WRONLY;
	
	int db_fd = open(db_dir, open_flags, 0);
	if (db_fd < 0){
		db_server_errno = DB_DOES_NOT_EXIST;
		return -1;
	}

	// We move the file's cursor to the end of the file.
	lseek(db_fd, 0, SEEK_END);

	// We forge the new entry and write it in the file.
	write(db_fd, key, strlen(to_delete->key));
	write(db_fd, ":", 1);
	write(db_fd, TOMBSTONE_DELETE, strlen(TOMBSTONE_DELETE));
	write(db_fd, "\n", 1);

	// We free the memory from deleted key
	free(to_delete->key);
	free(to_delete);

	close(db_fd);

	return 0;
}
// --------------------------------------------------------------------------------
int close_db(const char* db_dir){
	
	db_server_errno = 0;

	if (db_dir == NULL){
		db_server_errno = NULL_DB_DIR;
		return -1;
	}

	Databases_Hash* dbh = NULL;

	// First we check whether database is loaded.
	HASH_FIND( hh, db_hash_table, db_dir, strlen(db_dir) + 1, dbh);

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;		
	}

	// If it's loaded, we iterate over all items, cleaning as we go.
	Entries_Hash* queried_table = dbh->entries_hash_table;

	Entries_Hash* it = queried_table;
	while (it != NULL){
		Entries_Hash* next = it->hh.next;

		HASH_DEL(queried_table, it);
		free(it->key);
		free(it);	

		it = next;
	}
	
	HASH_DEL(db_hash_table, dbh);
	
	free(dbh->db_dir);
	free(dbh);

	return 0;
}

// --------------------------------------------------------------------------------
int print_all_items(const char* db_dir){

	Databases_Hash* dbh = NULL;

	// First we check whether database is already loaded.
	HASH_FIND_STR( db_hash_table, db_dir, dbh );

	if (!dbh){
		db_server_errno = DB_NOT_LOADED;
		return -1;		
	}

	Entries_Hash* queried_table = dbh->entries_hash_table;

	char* value_mem_ptr = NULL;
	for (Entries_Hash* it = queried_table->hh.next; it != NULL; it = it->hh.next){
		get_val(db_dir, it->key, &value_mem_ptr);
		printf("KEY: %s, OFFSET: %lu, VALUE: %s\n", 
				it->key, it->offset, value_mem_ptr);
	}

	return 0;

}












