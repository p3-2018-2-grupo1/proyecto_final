all: ./bin/logdb ./bin/client-cli

./bin/logdb: ./src/logdb.c ./src/db_server_errors.c ./src/database.c
	gcc -Wall -g $^ -Iinclude -o $@

./bin/client-cli: ./lib/liblogdb.so ./src/client-cli.c ./src/db_server_errors.c ./src/db_client_errors.c ./src/client-logdb.c
	gcc -Wall -g $^ -Iinclude ./lib/liblogdb.so -o $@

./lib/liblogdb.so: ./src/client-logdb.c
	gcc -Wall -fPIC -shared -I ./include/ ./src/client-logdb.c -o $@

# test_hashtable: src/test_hashtable.c
# 	gcc  -Iinclude/ src/test_hashtable.c -o bin/test_hashtable

.PHONY: clean
clean:
	rm -rf bin/ obj/
	mkdir bin/ obj/
	mkdir bin/db
